#ifndef TEMPERAUTRE_LOOKUP_TABLE_H
#define TEMPERAUTRE_LOOKUP_TABLE_H

#include<Vector.h>
#include "./utils.h"

struct TempLookupTableEntry{
  //In degree C
  float temp;
  //In Percen 0.0-1.0
  float power;
};

TempLookupTableEntry _fan1Lookup[] = {
  {5, 0},
  {20, 0},
  {22, .2},
  {25, .3},
  {30, .5},
  {35, .8},
  {37, 1.0},
  {50, 1.0}
};

Vector<TempLookupTableEntry> Fan1Lookup(_fan1Lookup, 8);

TempLookupTableEntry _fan2Lookup[] = {
  {5, 0},
  {27, 0},
  {28, 0.1},
  {29, 0.3},
  {32, 0.7},
  {37, 1.0},
  {50, 1.0}
};

Vector<TempLookupTableEntry> Fan2Lookup(_fan2Lookup, 7);


float TempLookupTableGetPower(float temp, Vector<TempLookupTableEntry> &table){
  float lastTemp = 0, lastPower = 0;
  TempLookupTableEntry last, current;
  last = {0, 0};
  

  for(int tableIndex = 0; tableIndex < table.size(); tableIndex++){
    current = table[tableIndex];
    if(current.temp > temp){
      break;
    }
    last = current;
  }
  if(last.temp == current.temp){
    return last.power;
  }

  return scaleTo(temp, last.temp, current.temp, last.power, current.power);
}

#endif