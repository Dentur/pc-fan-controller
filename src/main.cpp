
//#define USE_DEBUG_SERIAL
//#define USE_TEMPERATURE_DEBUG_CYCLING

#include <Arduino.h>
#include <pins_arduino.h>
#include "./fanControll.h"
#include "./temperatureUtils.h"
#include "./displayController.h"
#include "./TemperatureLookupTable.h"



bool prescalar = false;

void setup() {


  pinMode(LED_BUILTIN, OUTPUT);

  setupFanControll();
  setupTemperatureUtils();
  setupDisplayController();

  #ifdef USE_DEBUG_SERIAL
  Serial.begin(9600);
  while (!Serial)
    delay(10);
  
  Serial.println("Rack Cooler Debug Mode!");
  #endif

}

float temperature = 5;
#ifdef USE_TEMPERATURE_DEBUG_CYCLING
unsigned long lastMilies = 0;
#endif

enum RenderDisplays {
  RENDER_DISPLAY_TEMPERATURE,
  RENDER_DISPLAY_LOOKUP_FAN1,
  RENDER_DISPLAY_LOOKUP_FAN2,
  RENDER_DISPLAY_POWER_FAN1,
  RENDER_DISPLAY_POWER_FAN2
};

RenderDisplays currentDisplay = RENDER_DISPLAY_TEMPERATURE;
unsigned long currentDisplayTime = 0;
const unsigned long changeDisplayTime = 50; // 500;

void loop() {
  while(true){
    
    //Get current Temperature
    #ifndef USE_TEMPERATURE_DEBUG_CYCLING
    temperature = getTemperature();
    #else
    if((millis() - lastMilies) > 250){
      temperature += 0.25;
      if(temperature > 50)
      {
        temperature = 5;
      }
      lastMilies = millis();
    }
    #endif

    //Caclulate fan Speeds
    float fan1Power = TempLookupTableGetPower(temperature, Fan1Lookup);
    float fan2Power = TempLookupTableGetPower(temperature, Fan2Lookup);

    //Set desired Fan Speeds for temperature
    setFanSpeeds(fan1Power, fan2Power); 

    //Calculate current Fan Speeds

    //Update Display (maybe in different loop than get temperature?)
    switch(currentDisplay){
      case RENDER_DISPLAY_TEMPERATURE:
        renderTemperature(temperature, "Internal Temperature");
        if(currentDisplayTime >= changeDisplayTime){
          currentDisplay = RENDER_DISPLAY_LOOKUP_FAN1;
          currentDisplayTime = 0;
        }
      break;
      case RENDER_DISPLAY_LOOKUP_FAN1:
        renderLookupTable(Fan1Lookup, temperature, "Fan Group 1");
        if(currentDisplayTime >= changeDisplayTime){
          currentDisplay = RENDER_DISPLAY_LOOKUP_FAN2;
          currentDisplayTime = 0;
        }
      break;
      case RENDER_DISPLAY_LOOKUP_FAN2:
        renderLookupTable(Fan2Lookup, temperature, "Fan Group 2");
        if(currentDisplayTime >= changeDisplayTime){
          currentDisplay = RENDER_DISPLAY_POWER_FAN1;
          currentDisplayTime = 0;
        }
      break;
      case RENDER_DISPLAY_POWER_FAN1:
        renderFanPower(fan1Power, "Fan Group 1");
        if(currentDisplayTime >= changeDisplayTime){
          currentDisplay = RENDER_DISPLAY_POWER_FAN2;
          currentDisplayTime = 0;
        }
      break;
      case RENDER_DISPLAY_POWER_FAN2:
        renderFanPower(fan2Power, "Fan Group 2");
        if(currentDisplayTime >= changeDisplayTime){
          currentDisplay = RENDER_DISPLAY_TEMPERATURE;
          currentDisplayTime = 0;
        }
      break;
    }
    currentDisplayTime += 1;
    

    #ifdef USE_DEBUG_SERIAL
    Serial.print("Current Temperature: ");
    Serial.print(temperature, 3);
    Serial.println(" degree C");
    #endif
    delay(200);
  }
}


/*
  DONE
  - Read Temperature Sensor(s?) from I2C
  - Display current Temperature to Display (I2C)
  - Create PWM signal on D5 and D9 (21kHz - 29kHz)
    - Use a Temperature to PWM curve for each pin
    - Set Correect Frequency
      - https://ww1.microchip.com/downloads/en/DeviceDoc/ATmega4808-09-DataSheet-DS40002173B.pdf seite 192
      - TCAn.PER => Resolutions ((two bits) 0x0002 - (16 bits) MAX -1)
      - Frequency depends on TCA_PER (period setting), system clock frequence (f CLK_PER), TCA prescaler (CLKSEL in TCAn.CTRAL)
      - Freqzency_PWM = f_CLK_PER / (N*(PER+1)) where n is the prescaler divider
      - Getting started with TCA / TCB
        - https://ww1.microchip.com/downloads/en/Appnotes/TB3217-Getting-Started-with-TCA-DS90003217.pdf
        - https://ww1.microchip.com/downloads/en/Appnotes/TB3214-Getting-Started-with-TCB-DS90003214.pdf

      - Split mode macht aus 16 bit Timer 2 8 bit Timer (mit je 3 PWM compare channels)
  TODO
  - Read Fan Speeds where available
    - RPM = frequency [Hz] × 60 ÷ 2 (https://noctua.at/pub/media/wysiwyg/Noctua_PWM_specifications_white_paper.pdf)
    - Get freqzency with interrups?
    - A3 = Fan 1
    - A2 = Fan 2
    - A1 = Fan 3
    - A0 = Fan 4
    - D3 = Fan 5
    - D2 = Fan 6
*/