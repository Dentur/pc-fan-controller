#ifndef UTILS_H
#define UTILS_H

#include <Arduino.h>

struct v2i{
    int x;
    int y;
};

struct v2f{
    float x;
    float y;
};

float scaleTo(float value, float minFrom, float maxFrom, float minTo, float maxTo){
    return (((value - minFrom) * (maxTo - minTo)) / (maxFrom - minFrom))  + minTo;
};

int scaleTo(float value, float minFrom, float maxFrom, int minTo, int maxTo){
    int result = scaleTo(value, minFrom, maxFrom, float(minTo), float(maxTo)) + 0.5;
    return result;
}

v2i scaleTo(v2f value, v2f minFrom, v2f maxFrom, v2i minTo, v2i maxTo){
    return {
        scaleTo(value.x, minFrom.x, maxFrom.x, minTo.x, maxTo.x),
        scaleTo(value.y, minFrom.y, maxFrom.y, minTo.y, maxTo.y)
    };
}

#endif