#ifndef DISPLAY_CONTROLLER_H
#define DISPLAY_CONTROLLER_H

#include <U8g2lib.h>
#include "./TemperatureLookupTable.h"
#include <Vector.h>
#include "./utils.h"

//#define USE_DISPLAY_CONTROLER_DEBUG_SERIAL

U8G2_SSD1306_128X64_NONAME_1_HW_I2C display(U8G2_R0);

void setupDisplayController(){
    display.begin();
}


void renderTemperature(float temperature, const char *displayName){
    display.firstPage();
    do{
        char tempBuffer[10];
        dtostrf(temperature, 3, 1, tempBuffer);

        display.setFont(u8g2_font_fub20_tf);
        String str = "";
        str.concat(tempBuffer);
        str.concat("\xb0");
        str.concat("C");
        display.drawStr((display.getDisplayWidth() / 2) - (display.getStrWidth(str.c_str()) / 2), 30 , str.c_str());
        
        display.setFont(u8g2_font_helvB08_tf);
        display.drawStr((display.getWidth() / 2) - (display.getStrWidth(displayName) / 2), 40, displayName);
    }while(display.nextPage());
}

void renderLookupTable(Vector<TempLookupTableEntry>& table, float currentTemperature, const char* displayText){
    #ifdef USE_DEBUG_SERIAL
    Serial.println("In renderLookupTable");
    #ifdef USE_DISPLAY_CONTROLER_DEBUG_SERIAL
    Serial.print(table.size());
    Serial.println(" Elements in lookup table");
    #endif
    #endif
    
    display.firstPage();
    do{
        //Render Lookup graph name

        const int textLeft = 2, textBottom = 10;

        display.setFont(u8g2_font_helvB08_tf);
        display.drawStr(textLeft, textBottom, displayText);

        int textWidth = display.getStrWidth(displayText);
        const int textRight = textLeft + textWidth;

        //Render Lookup Graph
        
        const int graphLeft = 0, graphBottom = 54, graphTop = 0, graphRight = 127;
        const float minPower = 0, maxPower = 1;
        const int minTemperature = 5, maxTemperature = 50;
        const int graphLeftOffset = graphLeft + 2, graphBottomOffset = graphBottom - 2, graphTopOffset = graphTop + 2, graphRightOffset = graphRight - 2;

        display.drawLine(graphLeft, graphBottom, graphRight, graphBottom);
        display.drawLine(graphLeft, graphTop, graphLeft, graphBottom);
        v2i lastPos = {graphLeftOffset, graphBottomOffset};
        for(int lookupEntryIndex = 0; lookupEntryIndex < table.size(); lookupEntryIndex++){
            TempLookupTableEntry entry = table.at(lookupEntryIndex);
            v2i currentPos = scaleTo({entry.temp, entry.power}, {minTemperature, maxPower}, {maxTemperature, minPower}, {graphLeftOffset, graphTopOffset}, {graphRightOffset, graphBottomOffset});
            #ifdef USE_DEBUG_SERIAL
            #ifdef USE_DISPLAY_CONTROLER_DEBUG_SERIAL
            Serial.print("Element temp=");
            Serial.print(entry.temp);
            Serial.print(" power");
            Serial.println(entry.power);
            Serial.print("Drawing from x=");
            Serial.print(lastPos.x);
            Serial.print(" y=");
            Serial.print(lastPos.y);
            Serial.print(" to x=");
            Serial.print(currentPos.x);
            Serial.print(" y=");
            Serial.println(currentPos.y);
            #endif
            #endif
            display.drawLine(lastPos.x, lastPos.y, currentPos.x, currentPos.y);
            lastPos.x = currentPos.x;
            lastPos.y = currentPos.y;
        }
        display.drawLine(lastPos.x, lastPos.y, graphRightOffset, graphTopOffset);

        //Render current temperature triangle
        const int temperatureTriangleOffset = 3, temperatureTriangleHeight = 8;
        int temperaturePosition = scaleTo(currentTemperature, minTemperature, maxTemperature, graphLeftOffset, graphRightOffset);
        display.drawTriangle(temperaturePosition, graphBottom + 1, 
            temperaturePosition - temperatureTriangleOffset, graphBottom + 1  + temperatureTriangleHeight,
            temperaturePosition + temperatureTriangleOffset, graphBottom + 1 + temperatureTriangleHeight);

        //Render temperature line
        int temperatureLineYPos = 0;
        const int temperatureLineSegmentHeight = 5;

        // - Do not draw throug the display text
        if((temperaturePosition > (textLeft - 1)) && (temperaturePosition < (textRight+1))){
            temperatureLineYPos = textBottom + 2;    
        }
        // - Render a segmented vertical line
        while(temperatureLineYPos < display.getDisplayHeight()){
            int lineHeight = temperatureLineSegmentHeight;
            if((temperatureLineYPos + lineHeight) > graphBottom){
                lineHeight = abs(graphBottom - temperatureLineYPos);
            }
            display.drawVLine(temperaturePosition, temperatureLineYPos, lineHeight);
            temperatureLineYPos += 2*temperatureLineSegmentHeight;
        }

        //Render current Temperature
        String currentTempStr;
        char tempBuffer[10];
        dtostrf(currentTemperature, 3, 1, tempBuffer);
        currentTempStr.concat(tempBuffer);
        currentTempStr.concat("\xb0");
        currentTempStr.concat("C");
        display.setFont(u8g2_font_helvB08_tf);
        int temperatureTextPosition = temperaturePosition + temperatureTriangleOffset + 2;
        int temperatureTextWidth = display.getStrWidth(currentTempStr.c_str());
        if((temperatureTextPosition + temperatureTextWidth) > display.getDisplayWidth()){
            temperatureTextPosition = temperaturePosition - temperatureTriangleOffset - 2 - temperatureTextWidth;
        }
        display.drawStr(temperatureTextPosition, graphBottom + 1 + 8, currentTempStr.c_str());
    }while(display.nextPage());
}

void renderFanPower(float fanPower, const char* displayName){
    display.firstPage();
    do{
        char powerBuffer[10];
        dtostrf(fanPower*100, 4, 1, powerBuffer);
        String fanPowerString;
        fanPowerString.concat(powerBuffer);
        fanPowerString.concat("%");
        
        //display.setFont(u8g2_font_ncenB14_tr);
        display.setFont(u8g2_font_fub20_tf);
        display.drawStr((display.getWidth() / 2) - (display.getStrWidth(fanPowerString.c_str()) / 2), 30, fanPowerString.c_str());
        display.setFont(u8g2_font_helvB08_tf);
        display.drawStr((display.getWidth() / 2) - (display.getStrWidth(displayName) / 2), 40, displayName);

        //Render Power Indicator bar
        display.drawFrame(10, 47, 108, 17);
        display.drawBox(12, 49, scaleTo(fanPower, 0, 1, 0, 128 - 12 - 12), 13);
    }while(display.nextPage());
}

#endif