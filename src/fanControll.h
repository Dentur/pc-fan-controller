#ifndef FAN_CONTROLLER_H
#define FAN_CONTROLLER_H

#include <Arduino.h>
#include "./utils.h"


#define PIN_PWM_FAN_GROUP_1 5
#define PIN_PWM_FAN_GROUP_2 9


void setupFanControll(){

  pinMode(PIN_PWM_FAN_GROUP_1, OUTPUT);
  pinMode(PIN_PWM_FAN_GROUP_2, OUTPUT);

  //Set PWM to ~25kHz
  TCA0.SINGLE.CTRLA = (TCA_SINGLE_CLKSEL_DIV2_gc) | (TCA_SINGLE_ENABLE_bm);
  TCA0.SINGLE.PER = 300;

  //Alternativ, which reduces resolution but does not mess with the timers
  //TCA0.SINGLE.PER = 10;

  //Initialy set fans to 50% Fans 
  //Note setting this to 0 will disable the output, for some reason.
  analogWrite(PIN_PWM_FAN_GROUP_1, 150);
  analogWrite(PIN_PWM_FAN_GROUP_2, 150);
}

void setFanSpeeds(float power1, float power2){
  int scaledPower1 = scaleTo(power1, 0, 1, 0, 300);
  int scaledPower2 = scaleTo(power2, 0, 1, 0, 300);
  TCA0.SINGLE.CMP2BUF = scaledPower1; //D5
  TCA0.SINGLE.CMP1BUF = scaledPower2; //D9
  TCA0.SINGLE.CMP0BUF = scaledPower1; //D8
}



#endif