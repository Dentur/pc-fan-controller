#ifndef TEMPERATURE_UTILS_H
#define TEMPERATURE_UTILS_H

#include <Adafruit_AHTX0.h>

Adafruit_AHTX0 aht;
Adafruit_Sensor *humiditySensor, *temperatureSensor;

void setupTemperatureUtils(){
    if(!aht.begin()){
        //Could not Start AHT
        digitalWrite(LED_BUILTIN, 1);
    };
    humiditySensor = aht.getHumiditySensor();
    temperatureSensor = aht.getTemperatureSensor();
}

float getTemperature(){
    sensors_event_t temperatureEvent;
    temperatureSensor->getEvent(&temperatureEvent);
    return temperatureEvent.temperature;
}

float getHumidity(){
    sensors_event_t humidityEvent;
    humiditySensor->getEvent(&humidityEvent);
    return humidityEvent.relative_humidity;
}


#endif