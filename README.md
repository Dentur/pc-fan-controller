# PC Fan Controller
A Fan Controller that can control up to 8 PC Fans in two Groups. Comes with this Library, Files for the PCB, part lists and Models for mounting Hardware.


The Fan Controller measures the current temperature with a external I2C temperature Probe. The fan speeds for both fan Groups are calculated and set. Additionally multiple statistics are shown on the display.

Originally this Controller was designed for a Rack Cooling unit which can be mounted at the top of a 19" Rack. But it can also be used in many other projects which require 12V PWM controlled PC fans.

> Note: This project requires a Arduino Nano Every. Other Arduino microcontrollers are not compatible, because the procedure to change the PWM frequency is different.

## PCB Files
>Use the PCB design at your own risk

![PCB Render TOP](./resources/pcb-render-top.png)

The controller PCB was designed with [EasyEDA](https://easyeda.com/) and the layout and gerber files can be found in the `resources\pcb` directory.

### BOM
- 1-8 x 12V PC Fans: e.g. [Noctua A12x25](https://noctua.at/en/products/fan/nf-a12x25-pwm) but every other 12V 4Pin PC Fan should also work
- 1 x [Arduino Nano Every](https://docs.arduino.cc/hardware/nano-every): (as it can be used with 12V input voltage 
- 1 x [AHT20] (https://www.adafruit.com/product/4566)and outputs the required 5V PWM signal)
- 1 x SSD1306 128X64 Display
- 8 x Molex 470531000 - To connect the PC Fans
- 1 x 2 Pin PCB Terminal Block: e.g. KF128-5.08-2P
- 8 x 2.7K Ohm Resistors: As recommended by [Noctuas PWM Reference](https://noctua.at/pub/media/wysiwyg/Noctua_PWM_specifications_white_paper.pdf)
- 2 x 15 Pin Socket Headers (optional): To mount the Arduino
- 3 x 4 Pin Socket Headers: To Connect I2C devices
- 1 x 12V DC Power Supply with enough Watt to power the connected Fans


## Software
To use this software, it most likely needs to be modified, build and uploaded.

### Modifying the temperature curves
The Fan Speeds are determined by two Lookup tables located in `src/TemperatureLookupTable.h`.

The values of `_fan1Lookup` and `_fan2Lookup` can be adjusted to suit the specific needs of the the user. Each entry has two values, the first one is the temperature in degree celsius, the second the corresponding Fan Power in the Range of 0.0-1.0. The table must be sorted ascending after the Temperature. Any actual Temperature not present in the Table will be determined linearly between the lower and higher temperature.

The `Vector<TempLookupTableEntry> FanXLookup(_fanXLookup, <new number>);` lines need need to be adjusted if the amount of Entries are changed. `<new number>` must be set to the corresponding amount of entries!

### Building
To build the Software, the following libraries are required:
- [adafruit/Adafruit AHTX0](https://github.com/adafruit/Adafruit_AHTX0)
- [olikraus/U8g2](https://github.com/olikraus/u8g2)
- [janelia-arduino/Vector](https://github.com/janelia-arduino/Vector)

Then the Software can be build.

> Note: The `src/main.cpp` File contains some debug definitions, that can be commented in.
> 
> `USE_DEBUG_SERIAL`: Output some debug information to the Serial Port
>
> `USE_TEMPERATURE_DEBUG_CYCLING`: Instead of using the measured temperature, instead the temperature will be "slowly" increased of time, and reset if it reaches 50°C. This can be especially handy to test fan setups or new visualizations.

### Upload
The Arduino can be Flashed while mounted in the controller, but disconnecting from 12V Power is required.

### A note on timings / delay / millies
Any call to `millies` and `delay` will not return or use "normal" millie seconds. As the PWM frequency was changed by adjusting the clock divider, all timings are off by some factor.

### Modifying the Visualization
The displayed texts and visualization order can be easily changed.

The duration of each Screen is determined by `changeDisplayTime` in `src/main.cpp`. Do note, that the time is not in ms but in "cycles".

To disable a specific screen, find the `switch(currentDisplay){` switch block in `src/main.cpp`. Comment out the specific case and change the previous `currentDisplay =` assignment.

The displayed Texts can also be changed in this switch block.

Adding visualizations is more involved. See `src/displayController.h` for the implementation of the current visualizations.

### Using a larger / smaller display
Using a differently sized display is currently not supported, but pull requests (or forks) are welcome. This is due to the fact, that each visualization was hand crafted for that specific resolution.

## TODO
- Read Fan Speeds where available and visualize them
  - Add a warning if Fans no longer Work as expected